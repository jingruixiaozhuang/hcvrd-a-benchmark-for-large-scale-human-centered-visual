# Introduction

***If you use this dataset in your research, please cite our paper:***

```
@article{zhuang2017care,
  title={Care about you: towards large-scale human-centric visual relationship detection},
  author={Zhuang, Bohan and Wu, Qi and Shen, Chunhua and Reid, Ian and Hengel, Anton van den},
  journal={arXiv preprint arXiv:1705.09892},
  year={2017}
}
```

## Dataset
Our HCVRD dataset is based on the Visual Genome dataset. Please download the dataset at http://visualgenome.org/api/v0/api_home.html

```
* final_data.json: the json file including the annotations for the whole dataset
  
* long_tail_indexes.mat: provides the indexes for the long tail relationships

* train.txt and test.txt: provides the training and testing splits

* zeroshot_data.json: provide the zeroshot data annotations

```

  

## Copyright

Copyright (c) Bohan Zhuang. 2017

** This code is for non-commercial purposes only. For commerical purposes,
please contact Bohan Zhuang <bohan.zhuang@adelaide.edu.au> **

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

